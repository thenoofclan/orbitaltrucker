# orbitaltrucker
A slower game about space trucking, no longer just for GBJAM5
see [here](https://github.com/TheNOOFClan/Orbital_Trucker-discontinued) for the prototype

# Constrictions
This version will be constricted to the nes hardware specifications (as to keep our artist(s) from getting a headache). keep in mind this is only for outputs not really resources available.

Colors: 48 colors & 6 grays, as seen [here](http://www.thealmightyguru.com/Games/Hacking/Wiki/index.php?title=NES_Palette)

Resolution: 256X240

Sound: 5 channels, may use Konami VRC6, VRC7, etc.


# Building
This git repo only contains source code; to build, one must provide the resources listed in `core/assets/READ.ME`.

# Design Docs
Documents that dictate the desired design of the [Engine](https://docs.google.com/document/d/1QG2NCtJzbCzGLlANXVSPHpsLtg4CUGJitQmxb_Um8cs/edit?usp=sharing) and the [Game](https://docs.google.com/document/d/1dkhyBD_ifLPvFVszVcNMyK_wJWBTgUl2EnzgFYgEnok/edit?usp=sharing) can be found here