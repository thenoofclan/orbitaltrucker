package com.github.thenoofclan.orbitaltrucker.objects;

import java.util.Map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.github.thenoofclan.orbitaltrucker.util.JsonReader.JsonObject;

public class Ship extends PhysicsObject
{
	public Ship(float x, float y, float dir, Texture straight, Texture angle)
	{
		super(x, y, dir, straight, angle);
	}
	
	public Ship(JsonObject definition)
	{
		Map<String, JsonObject> converted = definition.toMap();
		x = converted.get("x").toFloat();
		y = converted.get("y").toFloat();
		dir = converted.get("dir").toInt() * 45;
		
		String texturePath = converted.get("texture").toString();
		String texture45path = converted.get("texture45").toString();
		
		straightImg = new Texture(Gdx.files.internal(texturePath));
		angleImg = new Texture(Gdx.files.internal(texture45path));
		
		this.define(x, y, dir, straightImg, angleImg);
		
	}
	
}
