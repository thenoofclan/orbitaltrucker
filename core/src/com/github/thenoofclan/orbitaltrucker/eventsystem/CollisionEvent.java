package com.github.thenoofclan.orbitaltrucker.eventsystem;

import com.github.thenoofclan.orbitaltrucker.objects.PhysicsObject;

public class CollisionEvent extends Event
{
	public PhysicsObject object1;
	public PhysicsObject object2;
	
	public CollisionEvent(PhysicsObject one, PhysicsObject two)
	{
		object1 = one;
		object2 = two;
		// very descriptive, I know
	}
}
