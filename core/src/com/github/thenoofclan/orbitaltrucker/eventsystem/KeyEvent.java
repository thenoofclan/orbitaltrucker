package com.github.thenoofclan.orbitaltrucker.eventsystem;

public class KeyEvent extends Event
{
	public int key;
	
	public KeyEvent(int key)
	{
		this.key = key;
	}
}
