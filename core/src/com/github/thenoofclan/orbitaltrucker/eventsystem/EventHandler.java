package com.github.thenoofclan.orbitaltrucker.eventsystem;

import java.util.LinkedList;
import java.util.List;

import com.github.thenoofclan.orbitaltrucker.objects.PhysicsObject;

public class EventHandler
{
	public static final int EVENT_COLLISION = 0b00000001;
	public static final int EVENT_KEY = 0b00000010;
	
	private List<KeyEventListener> keyListeners;
	private List<CollisionEventListener> collisionListeners;
	
	public EventHandler()
	{
		keyListeners = new LinkedList<KeyEventListener>();
		collisionListeners = new LinkedList<CollisionEventListener>();
	}
	
	public void register(EventListener l, int events)
	{
		if (events >= EVENT_KEY)
		{
			events -= EVENT_KEY;
			keyListeners.add((KeyEventListener) l);
		}
		if (events >= EVENT_COLLISION)
		{
			events -= EVENT_COLLISION;
			collisionListeners.add((CollisionEventListener) l);
		}
	}
	
	public void fireCollisionEvent(PhysicsObject object1, PhysicsObject object2)
	{
		CollisionEvent e = new CollisionEvent(object1, object2);
		for (CollisionEventListener l : collisionListeners)
		{
			l.onCollisionEvent(e);
		}
	}
	
	public void fireKeyEvent(int keycode)
	{
		KeyEvent e = new KeyEvent(keycode);
		for (KeyEventListener l : keyListeners)
		{
			l.onKeyEvent(e);
		}
	}
}
