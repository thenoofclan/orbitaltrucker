package com.github.thenoofclan.orbitaltrucker.eventsystem;

public interface CollisionEventListener extends EventListener
{
	void onCollisionEvent(CollisionEvent e);
}
