package com.github.thenoofclan.orbitaltrucker.eventsystem;

public interface KeyEventListener extends EventListener
{
	void onKeyEvent(KeyEvent e);
}
